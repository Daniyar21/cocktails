const express = require('express');
const multer = require('multer');
const path = require('path');
const {nanoid} = require('nanoid');
const config = require('../config');
const auth = require("../middleware/auth");
const permit = require("../middleware/permit");
const User = require("../models/User");
const Cocktail = require("../models/Cocktail");

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});

const router = express.Router();


router.get('/', async (req, res) => {
  console.log(req.headers);
  console.log('body', req.body);

  try {
    const authToken = req.get('Authorization');
    const user = await User.findOne({token: authToken});
    let cocktails = null;
    if (user && user.role === 'admin') {
      cocktails = await Cocktail.find();
      res.send(cocktails);
    } else {
      const searchParam = {published: true}
      cocktails = await Cocktail.find(searchParam);
      res.send(cocktails);
    }
    res.send(cocktails)
  } catch (e) {
    res.sendStatus(500);
  }
});


router.get('/mine', auth, async (req, res) => {
  const searchParam = {user: req.user._id}
  try {
    const cocktails = await Cocktail.find(searchParam);
    res.send(cocktails);
  } catch (e) {
    res.sendStatus(500);
  }
});


router.get('/:id', async (req, res) => {
  try {
    const oneCock = await Cocktail.findById(req.params.id);
    if (oneCock) {
      res.send(oneCock);
    } else {
      res.status(404).send({error: 'This cocktail is not found'});
    }
  } catch {
    res.sendStatus(500);
  }
});

router.post('/', auth, upload.single('image'), async (req, res) => {
  try {
    const cocktailData = {
      title: req.body.title,
      recipe: req.body.recipe,
      image: 'uploads/' + req.file.filename,
      ingredients: JSON.parse(req.body.ingredients),
      user: req.user._id
    };
    const cocktail = new Cocktail(cocktailData);
    await cocktail.save();
    res.send(cocktail);
  } catch (error) {
    res.status(400).send({error})
  }
});

router.delete('/:id', auth, permit('admin'), async (req, res) => {
  try {
    const cocktail = await Cocktail.findByIdAndDelete(req.params._id);
    if (!cocktail) {
      return res.status(404).send('Cocktail is not found');
    } else {
      res.send('Deleted successfully');
    }
  } catch (e) {
    res.status(500).send({message: "Something wrong in server"});
  }
});

router.post('/:id/publish', auth, permit('admin'), upload.single('image'), async (req, res) => {
  try {
    const cocktail = await Cocktail.findById(req.params.id);
    if (Object.keys(cocktail).length !== 0) {
      cocktail.published = !cocktail.published;
      await cocktail.save({validateBeforeSave: false});
      return res.send({message: "Cocktail was successfully published."})
    } else {
      return res.status(404).send({error: 'Cocktail is not found.'});
    }
  } catch (e) {
    res.status(400).send('Error');
  }
});

module.exports = router;