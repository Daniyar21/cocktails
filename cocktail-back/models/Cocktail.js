const mongoose = require("mongoose");


const IngredientsSchema = new mongoose.Schema({
  title:{
    type: String,
    required: true
  },
  amount:{
    type: String,
    required: true
  }
})
const CocktailSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true,
  },
  recipe: {
    type: String,
    required: true,
  },
  image: {
    type: String,
    required: true,
  },
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: true,
  },
  ingredients: [IngredientsSchema],
  published:{
    type: Boolean,
    default: false,
    required: true
  }
});

const Cocktail = mongoose.model('Cocktail', CocktailSchema);

module.exports = Cocktail;