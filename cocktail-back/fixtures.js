const mongoose = require('mongoose');
const {nanoid} = require('nanoid');
const config = require('./config');
const Cocktail = require("./models/Cocktail");
const User = require("./models/User");

const run = async () => {
  await mongoose.connect(config.db.url);
  const collection = await mongoose.connection.db.listCollections().toArray();
  for (const coll of collection) {
    await mongoose.connection.db.dropCollection(coll.name);
  }

  const [user1, user2] = await User.create({
    email: 'jack@gmail.com',
    password: '123',
    token: nanoid(),
    displayName: 'Jackie',
    role: 'admin',
  }, {
    email: 'sam@gmail.com',
    password: '123',
    token: nanoid(),
    displayName: 'Sammy',
    role: 'user',
  });

  await Cocktail.create({
    title: 'Blue Lagoon',
    image: 'fixtures/blue.jpg',
    ingredients: [
      {title: 'Lime juice', amount: '100 ml'},
      {title: 'Blue liqueur', amount: '200 ml'},
      {title: 'Vodka liqueur', amount: '200 ml'},
    ],
    recipe: 'How to make Blue lagoon',
    published: false,
    user: user1,

  },{
    title: 'Killer in Red',
    image: 'fixtures/red.jpeg',
    ingredients: [
      {title: 'Lime juice', amount: '100 ml'},
      {title: 'Red liqueur', amount: '200 ml'},
      {title: 'Vodka liqueur', amount: '200 ml'},
    ],
    recipe: 'How to make Killer in Red',
    published: false,
    user: user2,
  },{
    title: 'The Godfather',
    image: 'fixtures/godfather.jpg',
    ingredients: [
      {title: 'Lime juice', amount: '100 ml'},
      {title: 'Brown liqueur', amount: '200 ml'},
      {title: 'Vodka liqueur', amount: '200 ml'},
    ],
    recipe: 'How to make Godfather',
    published: false,
    user: user2,
  })

  await mongoose.connection.close();
};

run().catch(console.error);