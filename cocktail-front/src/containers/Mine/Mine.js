import React, {useEffect} from 'react';
import {Button, CircularProgress, Grid} from "@material-ui/core";
import {Link} from "react-router-dom";
import CocktailItem from "../../components/CocktailItem/CocktailItem";
import {useDispatch, useSelector} from "react-redux";
import {fetchMineRequest} from "../../store/actions/cocktailsActions";

const Mine = () => {

    const dispatch = useDispatch();
    const mine = useSelector(state => state.cocktails.mine);
    const fetchLoading = useSelector(state => state.cocktails.mineLoading);
    const user = useSelector(state => state.users.user.user);

    useEffect(() => {
        dispatch(fetchMineRequest());
    }, [dispatch]);

    return (
        <Grid container direction="column" spacing={2}>
            <Grid item container justifyContent="space-between" alignItems="center">

                {user && (
                    <Grid item>
                        <Button color="primary" component={Link} to="/cocktails/new">Add</Button>
                    </Grid>
                )}
            </Grid>
            <Grid item>
                <Grid item container justifyContent="center" direction="row" spacing={1}>
                    {fetchLoading ? (
                        <Grid container justifyContent="center" alignItems="center">
                            <Grid item>
                                <CircularProgress/>
                            </Grid>
                        </Grid>
                    ) : mine.map(cocktail => (
                        <CocktailItem
                            key={cocktail._id}
                            id={cocktail._id}
                            title={cocktail.title}
                            image={cocktail.image}
                            recipe={cocktail.recipe}
                        />
                    ))}
                </Grid>
            </Grid>
        </Grid>
    );
};

export default Mine;