import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {Link} from "react-router-dom";
import {Button, CircularProgress, Grid, makeStyles, Typography} from "@material-ui/core";
import {fetchCocktailsRequest} from "../../store/actions/cocktailsActions";
import CocktailItem from "../../components/CocktailItem/CocktailItem";

const useStyles = makeStyles(theme => ({
    title: {
        [theme.breakpoints.down('xs')]: {
            marginLeft: "50px",
        },
    }
}));

const MainPage = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const cocktails = useSelector(state => state.cocktails.cocktails);
    const fetchLoading = useSelector(state => state.cocktails.cocktailsLoading);
    const user = useSelector(state => state.users.user);

    console.log(cocktails);
    useEffect(() => {
        dispatch(fetchCocktailsRequest());
    }, [dispatch]);

    return (

            <Grid container direction="column" spacing={2}>
                <Grid item container justifyContent="space-between" alignItems="center">
                    <Grid item className={classes.title}>
                        <Typography variant="h4">All Cocktails</Typography>
                    </Grid>

                    {user && (
                        <Grid item>
                            <Button color="primary" component={Link} to="/cocktails/new">Add</Button>
                        </Grid>
                    )}
                </Grid>
                <Grid item>
                    <Grid item container justifyContent="center" direction="row" spacing={1}>
                        {fetchLoading ? (
                            <Grid container justifyContent="center" alignItems="center">
                                <Grid item>
                                    <CircularProgress/>
                                </Grid>
                            </Grid>
                        ) : cocktails.map(cocktail => (
                            <CocktailItem
                                key={cocktail._id}
                                id={cocktail._id}
                                title={cocktail.title}
                                image={cocktail.image}
                                recipe={cocktail.recipe}
                            />
                        ))}
                    </Grid>
                </Grid>
            </Grid>
    );
};

export default MainPage;