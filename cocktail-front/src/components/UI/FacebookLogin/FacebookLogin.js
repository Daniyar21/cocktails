import React from 'react';
import FacebookLoginButton from 'react-facebook-login/dist/facebook-login-render-props';
import {Button} from "@material-ui/core";
import FacebookIcon from '@material-ui/icons/Facebook';
import {facebookAppId} from "../../../config";
import {useDispatch} from "react-redux";
import {loginFacebook} from "../../../store/actions/usersActions";

const FacebookLogin = () => {
    const dispatch = useDispatch();

  const responseFacebook = response => {
    console.log(response);
      dispatch(loginFacebook(response));
  };

  return (
    <FacebookLoginButton
      appId={facebookAppId}
      fields="name,email,picture"
      render={props => (
        <Button
          fullWidth
          variant="outlined"
          color="primary"
          startIcon={<FacebookIcon/>}
          onClick={props.onClick}
        >
          Login with facebook
        </Button>
      )}
      callback={responseFacebook} />
  );
};

export default FacebookLogin;