import {
    Card,
    CardActions,
    CardContent,
    CardHeader,
    CardMedia,
    Grid,
    IconButton,
    makeStyles,
    Typography
} from "@material-ui/core";
import ArrowForwardIcon from "@material-ui/icons/ArrowForward";
import {Link} from "react-router-dom";
import {apiURL} from "../../config";

const useStyles = makeStyles({
    card: {
        height: '100%'
    },
    media: {
        height: 0,
        paddingTop: '56.25%'
    }
})

const CocktailItem = ({title, id, image}) => {
    const classes = useStyles();


    return (
        <Grid item xs={12} sm={12} md={6} lg={4}>
            <Card className={classes.card}>
                <CardHeader title={title}/>
                <CardMedia
                    image={ apiURL + '/' + image}
                    className={classes.media}
                />
                <CardContent>
                </CardContent>
                <CardActions>
                    <IconButton component={Link} to={'/cocktails/' + id}>
                        <ArrowForwardIcon />
                        See more
                    </IconButton>
                </CardActions>
            </Card>
        </Grid>
    );
};

export default CocktailItem;