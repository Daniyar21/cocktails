import Layout from "./components/UI/Layout/Layout";
import {Redirect, Route, Switch} from "react-router-dom";
import Login from "./containers/Login/Login";
import {useSelector} from "react-redux";
import Mine from "./containers/Mine/Mine";
import MainPage from "./containers/MainPage/MainPage";
import OneCocktail from "./containers/OneCocktail/OneCocktail";

const App = () => {
    const user = useSelector(state => state.users.user);

    const ProtectedRoute = ({isAllowed, redirectTo, ...props}) => {
        return isAllowed ?
            <Route {...props}/> :
            <Redirect to={redirectTo}/>
    };

    return (
        <Layout>
            <Switch>
               {/*// <ProtectedRoute path="/" exact isAllowed={user} redirectTo="/login" component={Events}/>*/}
                <Route path="/" exact component={MainPage}/>
                <Route path="/cocktail/:id" exact component={OneCocktail}/>
                <Route path="/login" component={Login}/>
                <Route path="/cocktails/mine" component={Mine}/>

            </Switch>
        </Layout>
    );
};

export default App;
