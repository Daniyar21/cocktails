import cocktailsSlice from "../slices/cocktailsSlice";

export const {
    fetchCocktailsRequest,
    fetchCocktailsSuccess,
    fetchCocktailsFailure,
    fetchMineRequest,
    fetchMineSuccess,
    fetchMineFailure,

} = cocktailsSlice.actions;

