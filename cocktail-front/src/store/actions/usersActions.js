import usersSlice from "../slices/usersSlice";

export const {
    registerUserRequest,
    registerUserSuccess,
    registerUserFailure,
    loginUserRequest,
    loginUserSuccess,
    loginUserFailure,
    loginFacebook,
    logoutRequest,
    logoutUser,
    clearErrorUser,
} = usersSlice.actions;






