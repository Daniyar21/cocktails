import {all} from 'redux-saga/effects';
import registerUserSaga from "./sagas/usersSagas";
import historySagas from "./sagas/historySagas";
import history from "../history";
import cocktailsSaga from "./sagas/cocktailsSagas";

export function* rootSagas() {
  yield all([
    ...registerUserSaga,
      ...cocktailsSaga,
    ...historySagas(history),

  ])
}