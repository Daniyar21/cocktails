import {createSlice} from "@reduxjs/toolkit";

export const initialState = {
    user: null,
    registerLoading: false,
    registerError: null,
    loginError: null,
    loginLoading: false,
};

const name = 'users';

const usersSlice = createSlice({
    name,
    initialState,
    reducers: {
        registerUserRequest(state, action) {
            state.registerLoading = true;
        },
        registerUserSuccess(state, {payload: userData}) {
            state.user = userData;
            state.registerLoading = false;
            state.registerError = null;
        },
        registerUserFailure(state, action) {
            state.registerLoading = false;
            state.registerError = action.payload;
        },
        loginUserRequest(state, action) {
            state.loginLoading = true;
        },
        loginUserSuccess(state, action) {
            state.loginLoading = false;
            state.loginError = null;
            state.user = action.payload;
        },
        loginUserFailure(state,action){
            state.loginLoading = false;
            state.loginError = action.payload;
        },
        loginFacebook(state, action) {
            state.loginLoading = true;
        },
        logoutRequest: () => {},
        logoutUser: (state) => {
            state.user = null;
        },
        clearErrorUser: (state) => {
            state.registerError = null;
            state.loginError = null;
        },
    }
});

export default usersSlice;

