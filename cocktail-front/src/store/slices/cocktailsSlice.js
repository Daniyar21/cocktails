import {createSlice} from "@reduxjs/toolkit";

const name = 'cocktails';

export const initialState = {
    cocktails: [],
    cocktailsLoading: false,
    mine: [],
    mineLoading: false,
    oneCock:[],
    oneLoading: false,
    addError: null,
    deleteLoading: false,
};

const cocktailsSlice = createSlice({
    name,
    initialState,
    reducers:{
        fetchCocktailsRequest(state){
            state.cocktailsLoading = true;
        },
        fetchCocktailsSuccess(state, action){
            state.cocktailsLoading = false;
            state.cocktails = action.payload;
        },
        fetchCocktailsFailure(state){
            state.cocktailsLoading = false;
        },
        fetchMineRequest(state,action){
            state.mineLoading = true;
        },
        fetchMineSuccess(state, {payload: mine}){
            state.mineLoading = false;
            state.mine = mine
        },
        fetchMineFailure(state,action){
            state.mineLoading = false;
        },
    }
});

export default cocktailsSlice;