import {put, takeEvery} from "redux-saga/effects";
import axiosApi from "../../axiosApi";
import {toast} from "react-toastify";
import {
    fetchCocktailsFailure,
    fetchCocktailsRequest,
    fetchCocktailsSuccess, fetchMineFailure, fetchMineRequest,
    fetchMineSuccess
} from "../actions/cocktailsActions";


export function* fetchCocktailsSaga() {
    console.log('in saga')
    try {
        const response = yield axiosApi.get('cocktails');
        console.log(response.data)
        yield put(fetchCocktailsSuccess(response.data));
    } catch (e) {
        toast.error('Could not fetch cocktails');
        yield put(fetchCocktailsFailure());
    }
}

export function* fetchMineSaga() {
    try {
        const response = yield axiosApi.get('cocktails/mine');
        yield put(fetchMineSuccess(response.data));
    } catch (e) {
        toast.error('Could not fetch your cocktails');
        yield put(fetchMineFailure());
    }
}

const cocktailsSaga = [
    takeEvery(fetchCocktailsRequest, fetchCocktailsSaga),
    takeEvery(fetchMineRequest, fetchMineSaga),
];

export default cocktailsSaga;