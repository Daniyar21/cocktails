import {takeEvery} from "redux-saga/effects";
import {
    loginUserFailure,
    loginUserRequest,
    loginUserSuccess,
    registerUserFailure,
    registerUserRequest,
    registerUserSuccess,
    loginFacebook, logoutRequest,
    logoutUser
} from "../actions/usersActions";
import axiosApi from "../../axiosApi";
import {historyPush} from "../actions/historyActions";
import {put} from 'redux-saga/effects';
import {toast} from "react-toastify";

export function* registerUserSaga({payload: userData}) {
    try {
        const response = yield axiosApi.post('/users', userData);
        yield put(registerUserSuccess(response.data));
        yield put(historyPush('/'));
        toast.success('Registered successful!');
    } catch (error) {
        toast.error(error.response.data.global);
        yield put(registerUserFailure(error.response.data));
    }
}

export function* loginUserSaga({payload: userData}) {
    try {
        const response = yield axiosApi.post('/users/sessions', userData);
        yield put(loginUserSuccess(response.data));
        yield put(historyPush('/'));
        toast.success('Login successful!');
    } catch (error) {
        toast.error(error.response.data.global);
        yield put(loginUserFailure(error.response.data));
    }
}

export function* facebookLoginSaga(action) {
    try {
        const response = yield axiosApi.post('/users/facebookLogin', action.payload);
        console.log('in saga response', response.data)
        yield put(loginUserSuccess(response.data.user));
        yield put(historyPush('/'));
        toast.success('Login successful!');
    } catch (error) {
        toast.error(error.response.data.global);
        yield put(loginUserFailure(error.response.data));
    }
}

export function* logoutUserSaga() {
    try {
        yield axiosApi.delete('/users/sessions');
        yield put(logoutUser());
        yield put(historyPush('/'));
        toast.success('Logout out!');
    } catch (error) {
        console.log(error)
        toast.error('Logout failed');
    }
}

const usersSaga = [
    takeEvery(registerUserRequest, registerUserSaga),
    takeEvery(loginUserRequest, loginUserSaga),
    takeEvery(loginFacebook, facebookLoginSaga),
    takeEvery(logoutRequest, logoutUserSaga),
];

export default usersSaga;